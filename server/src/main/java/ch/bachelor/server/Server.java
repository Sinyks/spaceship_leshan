/*
* Description a little leshan server to understand the leshan library and
* The OMA lwM2M Protocol
* Author: Sacha Perdrizat
*/

package ch.bachelor.server;

import org.eclipse.leshan.core.LwM2m;
import org.eclipse.leshan.core.LwM2mId;
import org.eclipse.leshan.core.model.LwM2mModel;
import org.eclipse.leshan.core.model.ObjectLoader;
import org.eclipse.leshan.core.model.ObjectModel;
import org.eclipse.leshan.core.node.LwM2mObject;
import org.eclipse.leshan.core.node.LwM2mObjectInstance;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.observation.Observation;
import org.eclipse.leshan.core.request.ContentFormat;
import org.eclipse.leshan.core.request.ReadRequest;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.server.californium.LeshanServer;
import org.eclipse.leshan.server.californium.LeshanServerBuilder;
import org.eclipse.leshan.server.model.LwM2mModelProvider;
import org.eclipse.leshan.server.model.StaticModelProvider;
import org.eclipse.leshan.server.registration.Registration;
import org.eclipse.leshan.server.registration.RegistrationListener;
import org.eclipse.leshan.server.registration.RegistrationUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class Server {

    private static final Logger LOG = LoggerFactory.getLogger(Server.class);

    private static int myObjectID = 11111;

    public static void main(String[] args) {

        // Basic Server Builder
        LeshanServerBuilder builder = new LeshanServerBuilder();

        // load the core models (Security, Server, Device ...)
        List<ObjectModel> models = ObjectLoader.loadDefault();

        // load sacha-IO object from models.
        String[] modelPaths = new String[] {"11111.xml"};
        models.addAll(ObjectLoader.loadDdfResources("/models/", modelPaths));

        // then add it to builder via the ModelProvider
        LwM2mModelProvider modelProvider = new StaticModelProvider(models);
        builder.setObjectModelProvider(modelProvider);

        LeshanServer server = builder.build();

        // add Event at registration

        server.getRegistrationService().addListener(new RegistrationListener() {
            @Override
            public void registered(Registration registration, Registration registration1,
                    Collection<Observation> collection) {
                    LOG.info("Oh Device {} Welcome aboard the ship",registration.getEndpoint());
                    LOG.info("try to read state of the reactor");
                try {
                    ReadRequest request = new ReadRequest(new ContentFormat(ContentFormat.TLV_CODE), myObjectID);
                    ReadResponse resp = server.send(registration,request);

                     Map<Integer,LwM2mResource> ressources = ((LwM2mObject) resp.getContent()).getInstance(0).getResources();

                    if (resp.isValid() && resp.isSuccess()){
                        LOG.info("Successfully read resources on object {}",registration.getEndpoint());

                        for (LwM2mResource ressource : ressources.values()) {
                            System.out.println(ressource);
                        }

                    }else {
                        LOG.error("failed to read error {} {}",resp.getCode(),resp.getErrorMessage());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();

                }

            }

            @Override
            public void updated(RegistrationUpdate registrationUpdate, Registration registration,
                    Registration registration1) {
                LOG.info("Oh Device {} Has Updated his status", registration.getEndpoint());
            }

            @Override
            public void unregistered(Registration registration, Collection<Observation> collection, boolean b,
                    Registration registration1) {
                LOG.info("Oh Device {} left us for a better ship",registration.getEndpoint());
            }
        });

        server.start();

    }
}
