# Spaceship leshan project

## TODO

- [x] Création d'un modèle OMA simple (Sacha-IO)
- [x] Implémenter le client leshan
- [x] Implémenter le serveur leshan
- [ ] Interface interactive (CLI) pour exploiter le serveur (Déclenchement de lecture/écriture/observation)

## Nice to do

- [ ] Implémenter le serveur de ``Bootstrap``
- [ ] Sécuriser les communications

