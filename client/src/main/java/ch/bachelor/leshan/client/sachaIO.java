package ch.bachelor.leshan.client;

import org.eclipse.leshan.client.resource.BaseInstanceEnabler;
import org.eclipse.leshan.client.servers.ServerIdentity;
import org.eclipse.leshan.core.model.ObjectModel;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.core.response.WriteResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.Destroyable;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class sachaIO extends BaseInstanceEnabler implements Destroyable {

    private static final Logger LOG = LoggerFactory.getLogger(sachaIO.class);

    private static final List<Integer> supportedResources = Arrays.asList(1, 2);
    private static final Random RANDOM = new Random();

    private boolean enableReactor;

    public sachaIO() {
        this.enableReactor = false;
    }

    @Override
    public ReadResponse read(ServerIdentity identity, int resourceid) {
        LOG.info("Read on Location resource /{}/{}/{}", getModel().id, getId(), resourceid);
        switch (resourceid){
            case 1:
                return ReadResponse.success(resourceid,this.enableReactor);

            case 2:
                return ReadResponse.success(resourceid, RANDOM.nextInt());
        }
        return ReadResponse.notFound();

    }

    @Override
    public WriteResponse write(ServerIdentity identity, int resourceid, LwM2mResource value) {
        LOG.info("Write on Location resource /{}/{}/{}", getModel().id, getId(), resourceid);

        if (resourceid == 1){
            this.enableReactor = ((boolean) value.getValue());
            return WriteResponse.success();
        }

        if (resourceid == 2){
            return WriteResponse.methodNotAllowed();
        }

        return WriteResponse.notFound();
    }

    @Override
    public List<Integer> getAvailableResourceIds(ObjectModel model) {
        return supportedResources;
    }
}
