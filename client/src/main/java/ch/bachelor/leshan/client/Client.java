package ch.bachelor.leshan.client;

import org.eclipse.leshan.client.californium.LeshanClient;
import org.eclipse.leshan.client.californium.LeshanClientBuilder;
import org.eclipse.leshan.client.object.Device;
import org.eclipse.leshan.client.object.Security;
import org.eclipse.leshan.client.object.Server;
import org.eclipse.leshan.client.resource.ObjectsInitializer;
import org.eclipse.leshan.core.LwM2mId;
import org.eclipse.leshan.core.model.LwM2mModel;
import org.eclipse.leshan.core.model.ObjectLoader;
import org.eclipse.leshan.core.model.ObjectModel;
import org.eclipse.leshan.core.model.StaticModel;
import org.eclipse.leshan.core.request.BindingMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;

public class Client {

    private static final Logger LOG = LoggerFactory.getLogger(Client.class);

    private static final String endpoint = "Reactor_" + UUID.randomUUID();
    private static final int myModelId = 11111;
    private static final String myModelName = "11111.xml";

    public static void main(String[] args) {

        LeshanClientBuilder builder = new LeshanClientBuilder(endpoint);

        // Load model mandatory + default
        List<ObjectModel> models = ObjectLoader.loadDefault();
        String[] modelPaths = new String[] {myModelName};
        models.addAll(ObjectLoader.loadDdfResources("/models/private", modelPaths));

        // add a custom object
        final LwM2mModel model = new StaticModel(models);
        ObjectsInitializer initializer = new ObjectsInitializer(model);

        // add Mandatory object ( Security (0), Server(1), Device(3))
        initializer.setInstancesForObject(LwM2mId.SECURITY, Security.noSec("coap://127.0.0.1:5683",12345));
        initializer.setInstancesForObject(LwM2mId.SERVER, new Server(12345, 5 * 60, BindingMode.U, false));

        initializer.setInstancesForObject(LwM2mId.DEVICE, new Device("Sacha Perdrizat", "model12345", "12345", "U"));

        initializer.setInstancesForObject(myModelId, new sachaIO());

        // add it to the client
        builder.setObjects(initializer.createAll());

        LeshanClient client = builder.build();

        LOG.info("Starting leshan client");

        client.start();
    }
}
