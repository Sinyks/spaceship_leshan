# Spaceship leshan project

## Objectif

Explorer les fonctionnalité de la library Eclipse Leshan en implémentant un Serveur et un Client lwM2M basique.

Un Modèle de donnée personnel a été crée pour l'occasion (en utilisant l'``ObjectID`` **11111**) sous le fichier ``11111.xml``avec [l'éditeur en ligne fourni par l'OMA](https://devtoolkit.openmobilealliance.org/OEditor/Legal?back=LWMOEdit)

## Compilation

A la racine du projet executer

```bash
mvn clean install
```

Les deux archives ``jar`` se trouveront dans leur répertoir ``target`` respectif.

**Client**

```bash
java -jar ./client/target/client*-jar-with-dependencies.jar
```

**Serveur**

```bash
java -jar ./server/target/server*-jar-with-dependencies.jar
```

## Leshan Demo Serveur

Pour une vue plus agréable et complête de l'objet on peut utiliser le serveur de demo de leshan (en y ajoutant notre modèle de donnée personnel).

```bash
wget https://ci.eclipse.org/leshan/job/leshan/lastSuccessfulBuild/artifact/leshan-server-demo.jar
java -jar ./leshan-server-demo.jar -m ./models/private

```
Et ainsi profiter de l'interface web disponible sur [http://localhost:8080](http://localhost:8080)

### Capture d'example

![](./images/leshan_Demo_server.png)
